import "./App.css";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Title from "./components/title/title";
import Cars from "./components/cars/cars";
import Rentals from "./components/rentals/rentals";

import { ToastContainer } from "react-toastify";

function App() {
  return (
    <>
      <Title />
      <Router>
        <Switch>
          <Route exact path="/">
            <Cars />
          </Route>
          <Route path="/:id/:name" component={Rentals} />
        </Switch>
      </Router>
      <ToastContainer />
    </>
  );
}

export default App;
