export function calculateAdditionalData(carsData) {
  let collection = 0;
  let availabilityList = {};

  carsData.forEach((carData) => {
    let isAvailable = true;
    collection += carData.rentals.reduce((prev, rental) => {
      if (!rental.returned) {
        isAvailable = false;
      }
      return prev + rental.duration_hours * rental.hourly_cost;
    }, 0);
    availabilityList[carData.id] = isAvailable;
  });

  return { collection, availabilityList };
}

export function calculateAvailability(rentalsData) {
  let isAvailable = true;
  rentalsData.forEach((rentalData) => {
    if (`${rentalData.returned}` === "false") {
      isAvailable = false;
    }
  });
  return isAvailable;
}

export function oneCarCollection(rentals) {
  let total = 0;
  rentals.forEach((rental) => {
    total += rental.hourly_cost * rental.duration_hours;
  });
  return total;
}
