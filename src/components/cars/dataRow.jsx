import React from "react";
import { NavLink } from "react-router-dom";

import CarModal from "./carModal";

class DataRow extends React.Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
    };
  }

  handleHide = () => {
    this.setState({ modalShow: !this.state.modalShow });
  };

  render() {
    let car = this.props.car;
    return (
      <tr>
        <td>{car.id}</td>
        <td>{car.name}</td>
        <td>{car.maker}</td>
        <td>{car.type}</td>
        <td>{this.props.availability ? "available" : "not available"}</td>
        <td>{car.seater}</td>
        <td>{car.model_year}</td>
        <td>{car.wheel_drive}</td>
        <td>
          <div>
            <button className="btn btn-warning btn-sm mx-2" onClick={this.handleHide}>
              Edit
            </button>
            <CarModal
              show={this.state.modalShow}
              action={"updateCar"}
              onHide={this.handleHide}
              onSubmit={this.props.onUpdate}
              car={car}
            ></CarModal>
            <button
              className="btn btn-danger btn-sm mx-2"
              onClick={() => this.props.onDelete(car.id)}
            >
              Delete
            </button>
            <NavLink to={`/${car.id}/${car.name}`} style={{ textDecoration: "none" }}>
              List Rentals
            </NavLink>
          </div>
        </td>
      </tr>
    );
  }
}

export default DataRow;
