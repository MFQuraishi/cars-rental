import React from "react";
import { Form, Table, Col, Row } from "react-bootstrap";

import * as rentalsApi from "../../services/carsApi";
import DataRow from "./dataRow";
import CarModal from "./carModal";
import { calculateAdditionalData } from "../../helperFunctions/helperFunctions";

import "./cars.css";

class Cars extends React.Component {
  constructor() {
    super();
    this.state = {
      cars: [],
      modalShow: false,
      searchParameter: "all",
      searchString: "",
    };
  }

  getCars = async () => {
    let cars = await rentalsApi.getAllCarsWithRentals();
    this.setState({ cars });
  };

  componentDidMount() {
    this.getCars();
  }

  handleHide = () => {
    this.setState({ modalShow: !this.state.modalShow });
  };

  handleSubmit = async (data) => {
    await rentalsApi.addNewCar(data);
    this.getCars();
  };

  handleDelete = async (id) => {
    let res = await rentalsApi.deleteCar(id);
    if (res.message.indexOf("successfully") > -1) {
      let newCars = this.state.cars.filter((car) => {
        return car.id !== id;
      });
      console.log("new cars", newCars);
      this.setState({ cars: newCars });
    }
  };

  handleUpdate = async (data) => {
    let id = data.id;
    delete data.id;

    const res = await rentalsApi.updateCar(data, id);

    data.id = id;
    if (res.message.indexOf("successfull" > -1)) {
      const newCarsState = this.state.cars.map((car) => {
        if (id === car.id) {
          return data;
        }
        return car;
      });
      this.setState({ cars: newCarsState });
    }
  };

  handleSearch = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    let additionalData = calculateAdditionalData(this.state.cars);

    return (
      <div className="bottom-section">
        <div className="cars-table-container">
          <div className="table-title mb-2">
            <h3>all Cars</h3>
            <Form onSubmit={this.handleSearchFormSubmit}>
              <Row>
                <Col>
                  <Form.Select
                    name={"searchParameter"}
                    type="select"
                    onChange={this.handleSearch}
                    value={this.state.searchParameter}
                  >
                    <option>all</option>
                    <option>name</option>
                    <option>maker</option>
                    <option>type</option>
                    <option>availability</option>
                    <option>seater</option>
                    <option>model_year</option>
                    <option>wheel_drive</option>
                  </Form.Select>
                </Col>
                <Col>
                  <Form.Control
                    name={"searchString"}
                    type="text"
                    onChange={this.handleSearch}
                    placeholder="Search String"
                    value={this.state.searchString}
                  />
                </Col>
              </Row>
            </Form>
            <button className="btn btn-primary mb-1" onClick={this.handleHide}>
              add car
            </button>
          </div>
          <CarModal
            show={this.state.modalShow}
            action={"addCar"}
            onHide={this.handleHide}
            onSubmit={this.handleSubmit}
          ></CarModal>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>id</th>
                <th>name</th>
                <th>maker</th>
                <th>type</th>
                <th>availability</th>
                <th>seater</th>
                <th>model year</th>
                <th>wheel drive</th>
                <th>actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.cars.map((car) => {
                let searchParameter = this.state.searchParameter;
                if (searchParameter === "all") {
                  return (
                    <DataRow
                      key={car.id}
                      car={car}
                      availability={additionalData.availabilityList[car.id]}
                      onDelete={this.handleDelete}
                      onUpdate={this.handleUpdate}
                    />
                  );
                }
                if (
                  searchParameter !== "availability" &&
                  `${car[searchParameter]}`
                    .toLowerCase()
                    .indexOf(`${this.state.searchString}`.toLowerCase()) > -1
                ) {
                  return (
                    <DataRow
                      key={car.id}
                      car={car}
                      availability={additionalData.availabilityList[car.id]}
                      onDelete={this.handleDelete}
                      onUpdate={this.handleUpdate}
                    />
                  );
                }
                if (
                  searchParameter === "availability" &&
                  additionalData.availabilityList[car.id] === true
                ) {
                  return (
                    <DataRow
                      key={car.id}
                      car={car}
                      availability={additionalData.availabilityList[car.id]}
                      onDelete={this.handleDelete}
                      onUpdate={this.handleUpdate}
                    />
                  );
                }
              })}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="9">total collection: {additionalData.collection}</td>
              </tr>
            </tfoot>
          </Table>
        </div>
      </div>
    );
  }
}

export default Cars;
