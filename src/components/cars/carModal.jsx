import React from "react";
import { Modal, Form, Button, Col, Row } from "react-bootstrap";
import { toast } from "react-toastify";
class CarModal extends React.Component {
  constructor(props) {
    super();

    if (props.action === "updateCar") {
      this.state = props.car;
    } else {
      this.state = {
        name: "",
        maker: "",
        type: "sedan",
        seater: "5",
        wheel_drive: "FWD",
        model_year: "",
      };
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    let rowData = this.state;
    let isValid = this.validateForm(rowData);

    if (isValid) {
      this.props.onSubmit(rowData);
      this.props.onHide();
      this.resetForm();
    } else {
      toast("Some fields are empty", {
        position: "bottom-center",
        draggable: true,
        type: "warning",
      });
      console.error("empty form fields");
    }
  };

  resetForm = () => {
    if (this.props.action !== "updateCar") {
      const initialState = {
        name: "",
        maker: "",
        type: "sedan",
        seater: "5",
        wheel_drive: "FWD",
        model_year: "",
      };

      this.setState({ ...initialState });
    }
  };

  validateForm = (data) => {
    let isValid = true;

    Object.values(data).forEach((element) => {
      if (!element) {
        isValid = false;
      }
    });
    if (isNaN(data.model_year)) {
      isValid = false;
    }

    return isValid;
  };

  render() {
    const carTypes = ["van", "pickup", "hatchback", "sport", "sedan", "SUV", "offroader"];
    const carSeaters = ["5", "2", "4", "6", "7", "8"];
    const wheelDrive = ["RWD", "FWD", "4WD", "AWD"];
    return (
      <Modal show={this.props.show} onHide={this.props.onHide} size="lg">
        <Modal.Header>
          <h3>{this.props.action}</h3>
        </Modal.Header>
        <Modal.Body className="px-3">
          <Form onSubmit={this.handleSubmit}>
            <Row>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    name={"name"}
                    type="text"
                    onChange={this.handleChange}
                    placeholder="name of car"
                    value={this.state.name}
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>Maker</Form.Label>
                  <Form.Control
                    type="text"
                    name="maker"
                    onChange={this.handleChange}
                    placeholder="Maker"
                    value={this.state.maker}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>type</Form.Label>
                  <Form.Select
                    name={"type"}
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.type}
                  >
                    {carTypes.map((type) => {
                      return <option>{type}</option>;
                    })}
                  </Form.Select>
                </Form.Group>
              </Col>
              <Col>
                <Form.Label>seater</Form.Label>
                <Form.Select
                  name={"seater"}
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.seater}
                >
                  {carSeaters.map((seater) => {
                    return <option>{seater}</option>;
                  })}
                </Form.Select>
              </Col>
            </Row>

            <Row className="mb-4">
              <Col>
                <Form.Label>wheel drive</Form.Label>
                <Form.Select
                  name="wheel_drive"
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.wheel_drive}
                >
                  {wheelDrive.map((drive) => {
                    return <option>{drive}</option>;
                  })}
                </Form.Select>
              </Col>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>model year</Form.Label>
                  <Form.Control
                    name={"model_year"}
                    type="number"
                    placeholder="model year"
                    onChange={this.handleChange}
                    value={this.state.model_year}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row className="px-2">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export default CarModal;
