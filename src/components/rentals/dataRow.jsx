import React from "react";

import RentalModal from "./rentalModal";

class DataRow extends React.Component {
  constructor() {
    super();
    this.state = {
      modalShow: false,
    };
  }

  handleHide = () => {
    this.setState({ modalShow: !this.state.modalShow });
  };

  render() {
    let rental = this.props.rental;
    return (
      <tr>
        <td>{rental.id}</td>
        <td>{rental.rented_by}</td>
        <td>{rental.duration_hours}</td>
        <td>{rental.hourly_cost}</td>
        <td>{`${rental.returned}`}</td>
        <td>{rental.hourly_cost * rental.duration_hours}</td>
        <td>{rental.additional_info}</td>
        <td>
          <div>
            <button className="btn btn-warning btn-sm mx-2" onClick={this.handleHide}>
              Edit
            </button>
            <RentalModal
              show={this.state.modalShow}
              action={"updateRental"}
              onHide={this.handleHide}
              onSubmit={this.props.onUpdate}
              rental={rental}
            ></RentalModal>
            <button
              className="btn btn-danger btn-sm mx-2"
              onClick={() => this.props.onDelete(rental.id)}
            >
              Delete
            </button>
          </div>
        </td>
      </tr>
    );
  }
}

export default DataRow;
