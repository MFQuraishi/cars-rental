import React from "react";

import { Table, Form, Row, Col } from "react-bootstrap";
import { toast } from "react-toastify";

import * as rentalsApi from "../../services/rentalsApi";
import DataRow from "./dataRow";
import RentalModal from "./rentalModal";
import { calculateAvailability } from "./../../helperFunctions/helperFunctions";
import { oneCarCollection } from "./../../helperFunctions/helperFunctions";

import "./../cars/cars.css";

class Rentals extends React.Component {
  constructor() {
    super();
    this.state = {
      rentals: [],
      modalShow: false,
      searchParameter: "all",
      searchString: "",
    };
  }

  getRentals = async () => {
    const carId = this.props.match.params.id;
    let carWithRentals = await rentalsApi.getAllRentals(carId);
    if (
      carWithRentals.message !== undefined &&
      carWithRentals.message.indexOf("Cannot find") > -1
    ) {
      console.error("no data found at id " + this.carId);
    } else {
      this.setState({ rentals: carWithRentals.rentals });
    }
  };

  componentDidMount() {
    this.getRentals();
  }

  handleHide = () => {
    this.setState({ modalShow: !this.state.modalShow });
  };

  handleSubmit = async (data) => {
    const rentalId = this.props.match.params.id;
    let newRental = await rentalsApi.addNewRental(rentalId, data);
    this.setState({ rentals: [...this.state.rentals, newRental] });
  };

  handleDelete = async (id) => {
    let res = await rentalsApi.deleteRental(id);
    if (res.message.indexOf("successfully") > -1) {
      let newRentals = this.state.rentals.filter((rental) => {
        return rental.id !== id;
      });
      console.log("new cars", newRentals);
      this.setState({ rentals: newRentals });
    }
  };

  handleUpdate = async (data) => {
    let id = data.id;
    delete data.id;

    const res = await rentalsApi.updateRental(data, id);

    data.id = id;
    if (res.message.indexOf("successfull" > -1)) {
      const newRentalsState = this.state.rentals.map((rental) => {
        if (id === rental.id) {
          return data;
        }
        return rental;
      });
      this.setState({ rentals: newRentalsState });
    }
  };

  showToast = (message) => {
    toast(message, {
      position: "bottom-center",
      draggable: true,
      type: "warning",
    });
  };

  handleSearch = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    let collection = oneCarCollection(this.state.rentals);
    return (
      <div className="bottom-section">
        <div className="cars-table-container">
          <div className="table-title">
            <h3>{this.props.match.params.name}</h3>
            <Form onSubmit={this.handleSearchFormSubmit}>
              <Row>
                <Col>
                  <Form.Select
                    name={"searchParameter"}
                    type="select"
                    onChange={this.handleSearch}
                    value={this.state.searchParameter}
                  >
                    <option>all</option>
                    <option>rented_by</option>
                    <option>duration_hours</option>
                    <option>hourly_cost</option>
                    <option>returned</option>
                    <option>amount_payable</option>
                  </Form.Select>
                </Col>
                <Col>
                  <Form.Control
                    name={"searchString"}
                    type="text"
                    onChange={this.handleSearch}
                    placeholder="Search String"
                    value={this.state.searchString}
                  />
                </Col>
              </Row>
            </Form>
            <button
              className="btn btn-primary mb-1"
              onClick={() =>
                calculateAvailability(this.state.rentals)
                  ? this.handleHide()
                  : this.showToast("car not available")
              }
            >
              {" "}
              Add new Rental
            </button>
          </div>
          <RentalModal
            show={this.state.modalShow}
            action={"addRental"}
            onHide={this.handleHide}
            onSubmit={this.handleSubmit}
          ></RentalModal>
          <Table striped hover variant="dark">
            <thead>
              <tr>
                <th>id</th>
                <th>rented by</th>
                <th>duration hours</th>
                <th>hourly cost</th>
                <th>returned</th>
                <th>amount payable</th>
                <th>additional info</th>
                <th>actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.rentals.map((rental) => {
                let searchParameter = this.state.searchParameter;
                if (searchParameter === "all") {
                  return (
                    <DataRow
                      key={rental.id}
                      rental={rental}
                      onDelete={this.handleDelete}
                      onUpdate={this.handleUpdate}
                    />
                  );
                }
                if (
                  searchParameter !== "amount_payable" &&
                  `${rental[searchParameter]}`
                    .toLowerCase()
                    .indexOf(`${this.state.searchString}`.toLowerCase()) > -1
                ) {
                  return (
                    <DataRow
                      key={rental.id}
                      rental={rental}
                      onDelete={this.handleDelete}
                      onUpdate={this.handleUpdate}
                    />
                  );
                }
                if (
                  searchParameter === "amount_payable" &&
                  `${rental.hourly_cost * rental.duration_hours}`
                    .toLowerCase()
                    .indexOf(`${this.state.searchString}`.toLowerCase()) > -1
                ) {
                  return (
                    <DataRow
                      key={rental.id}
                      rental={rental}
                      onDelete={this.handleDelete}
                      onUpdate={this.handleUpdate}
                    />
                  );
                }
              })}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="9">collection: {collection}</td>
              </tr>
            </tfoot>
          </Table>
        </div>
      </div>
    );
  }
}

export default Rentals;
