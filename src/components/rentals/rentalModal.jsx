import React from "react";
import { Modal, Form, Button, Col, Row, FloatingLabel } from "react-bootstrap";

import { toast } from "react-toastify";

class RentalModal extends React.Component {
  constructor(props) {
    super();

    if (props.action === "updateRental") {
      this.state = props.rental;
    } else {
      this.state = {
        rented_by: "",
        duration_hours: "",
        hourly_cost: "",
        returned: "false",
        additional_info: "",
      };
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    let rowData = this.state;
    let isValid = this.validateForm(rowData);

    if (isValid) {
      this.props.onSubmit(rowData);
      this.props.onHide();
      this.resetForm();
    } else {
      toast("Some fields are empty", {
        position: "bottom-center",
        draggable: true,
        type: "warning",
      });
      console.error("empty form fields");
    }
  };

  resetForm = () => {
    if (this.props.action !== "updateRental") {
      const initialState = {
        rented_by: "",
        duration_hours: "",
        hourly_cost: "",
        returned: "false",
        additional_info: "",
      };

      this.setState({ ...initialState });
    }
  };

  validateForm = (data) => {
    let isValid = true;
    let newData = { ...data };
    delete newData.additional_info;
    Object.values(newData).forEach((element) => {
      if (!`${element}`) {
        isValid = false;
      }
    });
    return isValid;
  };

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onHide} size="lg">
        <Modal.Header>
          <h3>{this.props.action}</h3>
        </Modal.Header>
        <Modal.Body className="px-3">
          <Form onSubmit={this.handleSubmit}>
            <Row>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>Rented By</Form.Label>
                  <Form.Control
                    name={"rented_by"}
                    type="text"
                    onChange={this.handleChange}
                    placeholder="Rented By"
                    value={this.state.rented_by}
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>Duration Hours</Form.Label>
                  <Form.Control
                    type="number"
                    name="duration_hours"
                    onChange={this.handleChange}
                    placeholder="Duration Hours"
                    value={this.state.duration_hours}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group className="mb-2">
                  <Form.Label>Hourly Cost</Form.Label>
                  <Form.Control
                    name={"hourly_cost"}
                    type="number"
                    placeholder="Hourly Cost"
                    onChange={this.handleChange}
                    value={this.state.hourly_cost}
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>Returned</Form.Label>
                  <Form.Select
                    name={"returned"}
                    type="select"
                    onChange={this.handleChange}
                    value={this.state.returned}
                  >
                    <option>false</option>
                    <option>true</option>
                  </Form.Select>
                </Form.Group>
              </Col>
            </Row>

            <FloatingLabel label="additional info" className="mb-3">
              <Form.Control
                onChange={this.handleChange}
                name="additional_info"
                as="textarea"
                placeholder="any additional info"
                value={this.state.additional_info}
              />
            </FloatingLabel>
            <Row className="px-2">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export default RentalModal;
