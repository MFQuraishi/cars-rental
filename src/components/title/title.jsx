import React from "react";

import "./title.css";

class Title extends React.Component {
  render() {
    return <div className="title">Rentals</div>;
  }
}

export default Title;
