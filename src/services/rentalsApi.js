export function updateRental(data, id) {
  const url = `https://single-entity-project.herokuapp.com/api/rentals/${id}`;
  const meta = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };
  return makeRequest(url, meta);
}

export function deleteRental(id) {
  const url = `https://single-entity-project.herokuapp.com/api/rentals/${id}`;
  const meta = {
    method: "DELETE",
  };

  return makeRequest(url, meta);
}

export function getAllRentals(id) {
  const url = `https://single-entity-project.herokuapp.com/api/cars/${id}/rentals/`;
  const meta = {
    method: "GET",
  };
  return makeRequest(url, meta);
}

export function addNewRental(rentalId, data) {
  console.log(rentalId, data);
  const url = `https://single-entity-project.herokuapp.com/api/rentals/${rentalId}`;
  const meta = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };
  return makeRequest(url, meta);
}

function makeRequest(url, meta) {
  return fetch(url, meta)
    .then((response) => response.json())
    .then((data) => data)
    .catch((e) => console.error(e));
}
