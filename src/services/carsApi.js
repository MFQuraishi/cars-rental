export function updateCar(data, id) {
  const url = `https://single-entity-project.herokuapp.com/api/cars/${id}`;
  const meta = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };
  return makeRequest(url, meta);
}

export function deleteCar(id) {
  const url = `https://single-entity-project.herokuapp.com/api/cars/${id}`;
  const meta = {
    method: "DELETE",
  };

  return makeRequest(url, meta);
}

export function getAllCars() {
  const url = "https://single-entity-project.herokuapp.com/api/cars/";
  const meta = {
    method: "GET",
  };
  return makeRequest(url, meta);
}
export function getAllCarsWithRentals() {
  const url = "https://single-entity-project.herokuapp.com/api/cars/rentals/";
  const meta = {
    method: "GET",
  };
  return makeRequest(url, meta);
}

export function addNewCar(data) {
  const url = "https://single-entity-project.herokuapp.com/api/cars";
  const meta = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };

  return makeRequest(url, meta);
}

function makeRequest(url, meta) {
  return fetch(url, meta)
    .then((response) => response.json())
    .then((data) => data)
    .catch((e) => console.error(e));
}
